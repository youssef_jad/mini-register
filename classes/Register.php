<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors', 1);
include '../config/db.php';

final class Register{

    public $db;
    public function __construct()
    {
        $this->db = connect();
    }

    public function validateInputs()
    {
        if(!isset($_POST['email'])  ||  $_POST['email'] == '')
        {
            $_SESSION['error_validation'] = 'Email Input Is Required';
            header('Location: /interview');
            exit;
        }

        if(!isset($_POST['password']) || $_POST['password'] == '')
        {
            $_SESSION['error_validation'] = 'Password Input Is Required';
            header('Location: /interview');
            exit;
        }
        return $this;
    }

    public function checkMail()
    {
        if(isset($_POST['email']) && $_POST['email'] != '')
        {
            $email = htmlentities($_POST['email']);
            $query =  $this->db->prepare('SELECT email FROM register WHERE email LIKE ?');
            $query->execute(array('%' . $email .'%'));
            $data = $query->fetch();
            if($data !== false )
            {
                $_SESSION['error_validation'] = 'Sorry This Email is already registered';
                header('Location: /interview');
                exit;
            }
            return $this;
        }
    }

    public function insert()
    {

        try{
            if(isset($_POST['email']) && $_POST['email'] != '')
            {
                $email = htmlentities($_POST['email']);
                $password = isset($_POST['password']) ? $_POST['password'] : random_int(111 , 4444 );
                $query =  $this->db->prepare('INSERT INTO `register` (`email`, `password`) VALUES (?,?) ');
                $query->execute([
                                    $email,
                                    $password
                                ]);
                $_SESSION['register_success'] = 'Successfully Registered';
                header('Location: /interview');
                exit;
            }
        }catch (Exception $e){
            $_SESSION['exception_message'] = $e->getMessage();
            header("Location: /interview");
            exit;
        }
    }
}

(new Register())->validateInputs()->checkMail()->insert();

?>